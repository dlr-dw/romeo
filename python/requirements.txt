capstone==4.0.2
scikit-learn==1.0
transformers[torch]==4.11.3
datasets==1.14.0
tqdm==4.62.3
ray[tune]==1.7.1
pyyaml==6.0
scipy==1.7.3