"""
transformer_pipeline_steps: Implementation of pipeline steps to train tokenizer and transformer models, and to obtain predictions.
"""

import logging
import typing

import numpy as np
from pipeline_framework import PipelineState, PipelineStep


class TrainTokenizerPipelineStep(PipelineStep):
    step_key = "train_tokenizer"

    def __init__(self, config: dict, state: PipelineState, logger: logging.Logger):
        super().__init__(config, state, logger)

        self.tokenizer_json_path = self.state.run_base_dir / "tokenizer.json"

    def input_ready(self, input: dict) -> bool:
        return "examples" in input.keys() and all(
            ["Example" in example.keys() for example in input["examples"]]
        )

    def execute(self, input: dict, output: dict) -> None:
        import tokenizers
        from tokenizers.models import BPE
        from tokenizers.pre_tokenizers import Whitespace
        from tokenizers.processors import BertProcessing
        from tokenizers.trainers import BpeTrainer

        tokenizer = tokenizers.Tokenizer(BPE(unk_token="<unk>"))

        tokenizer.enable_padding(pad_token="<pad>")
        tokenizer.enable_truncation(max_length=512)

        tokenizer.pre_tokenizer = Whitespace()

        # Build trainer
        trainer = BpeTrainer(
            special_tokens=["<unk>", "<sep>", "<mask>", "<pad>", "<s>", "</s>"]
        )

        # Load dataset
        train_snippets = [example["Example"] for example in input["examples"]]

        tokenizer.train_from_iterator(
            train_snippets, trainer=trainer, length=len(train_snippets)
        )

        tokenizer.post_processor = BertProcessing(
            ("</s>", tokenizer.token_to_id("</s>")),
            ("<s>", tokenizer.token_to_id("<s>")),
        )

        tokenizer.save(str(self.tokenizer_json_path), pretty=True)

        output["tokenizer_path"] = self.tokenizer_json_path

    def output_ready(self, output: dict) -> typing.Optional[bool]:
        return self.tokenizer_json_path.exists() and "tokenizer_path" in output.keys()


class TokenizeExamplesPipelineStep(PipelineStep):
    step_key = "tokenize_examples"

    def __init__(self, config: dict, state: PipelineState, logger: logging.Logger):
        super().__init__(config, state, logger)

        import yaml
        from transformers import BatchEncoding

        yaml.add_representer(BatchEncoding, lambda dumper, _: dumper.represent_int(0))

    def input_ready(self, input: dict) -> bool:
        return "examples" in input.keys() and "tokenizer_path" in input.keys()

    def execute(self, input: dict, output: dict) -> None:
        import classifier

        tokenizer_json_path = input["tokenizer_path"]
        tokenizer = classifier.tokenizer(tokenizer_json_path)

        lengths = []
        for example in input["examples"]:
            example["TokenizedExample"] = tokenizer(
                example["Example"], truncation=True, padding=True
            )
            lengths.append(len(example["TokenizedExample"].data["input_ids"]))

        self.logger.info(f"Max length: {max(lengths)}")
        self.logger.info(f"Min length: {min(lengths)}")
        self.logger.info(f"Avg length: {np.mean(lengths)}")
        output["examples"] = input["examples"]

    def output_ready(self, output: dict) -> typing.Optional[bool]:
        return "examples" in output.keys() and all(
            ["Example" in example.keys() for example in output["examples"]]
        )


class TrainClassifierPipelineStep(PipelineStep):
    step_key = "train_classifier"

    def __init__(self, config: dict, state: PipelineState, logger: logging.Logger):
        super().__init__(config, state, logger)

        # Set some paths
        self.output_dir = self.state.run_base_dir / "classifier"

    def input_ready(self, input: dict) -> bool:
        return (
            "examples" in input.keys()
            and "tokenizer_path" in input.keys()
            and "label_encoding" in input.keys()
        )

    def execute(self, input: dict, output: dict) -> None:
        import classifier
        from datasets import load_metric
        from transformers import Trainer

        train_dataset = classifier.build_huggingface_dataset(
            [ex for ex in input["examples"] if ex["Split"] == "Training"],
            input["label_encoding"],
        )
        eval_dataset = classifier.build_huggingface_dataset(
            [ex for ex in input["examples"] if ex["Split"] == "Validation"],
            input["label_encoding"],
        )

        model = classifier.model_init(
            input["label_encoding"], self.config["model_name"]
        )

        tokenizer_json_path = input["tokenizer_path"]
        tokenizer = classifier.tokenizer(tokenizer_json_path)
        data_collator = classifier.data_collator(tokenizer)
        training_args = classifier.training_arguments(
            self.output_dir, self.state.smoke_test
        )
        metric = load_metric("accuracy")

        trainer = Trainer(
            model=model,
            args=training_args,
            data_collator=data_collator,
            tokenizer=tokenizer,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            compute_metrics=lambda eval_pred, metric=metric: classifier.compute_metrics(
                metric, eval_pred
            ),
        )

        trainer.train()
        model.save_pretrained(self.output_dir)
        output["model_path"] = self.output_dir

    def output_ready(self, output: dict) -> typing.Optional[bool]:
        return self.output_dir.exists() and "model_path" in output.keys()


class PredictSplitPipelineStep(PipelineStep):
    step_key = "predict_split"

    def __init__(
        self, config: dict, state: PipelineState, logger: logging.Logger, split: str
    ):
        super().__init__(config, state, logger)

        self.split = split

    def input_ready(self, input: dict) -> bool:
        return "model_path" in input.keys() and "examples" in input.keys()

    def execute(self, input: dict, output: dict) -> None:
        import classifier

        test_examples = [ex for ex in input["examples"] if ex["Split"] == self.split]
        classifier.obtain_predictions(
            test_examples,
            input["tokenizer_path"],
            input["model_path"],
            smoke_test=self.state.smoke_test,
        )

        output["examples"] = input["examples"]


__all__ = [
    "TrainTokenizerPipelineStep",
    "TokenizeExamplesPipelineStep",
    "TrainClassifierPipelineStep",
    "PredictSplitPipelineStep",
]
