"""
    classifier: Configuration and logic encapsulating the transformer model.
"""

import numpy as np
import torch
from tokenizers import Tokenizer
from torch.utils.data import DataLoader
from transformers import (
    DataCollatorWithPadding,
    PreTrainedTokenizerFast,
    RobertaForSequenceClassification,
    TrainingArguments,
)


def training_arguments(output_dir: str, smoke_test: bool):
    """These are the hardcoded arguments for transformer training."""
    return TrainingArguments(
        output_dir=output_dir,
        evaluation_strategy="epoch",
        num_train_epochs=10 if not smoke_test else 1.0,
        logging_steps=100 if not smoke_test else 10,
        learning_rate=1.1e-5,
        weight_decay=3e-4,
        per_device_train_batch_size=16 if not smoke_test else 4,
        per_device_eval_batch_size=64 if not smoke_test else 4,
        disable_tqdm=not smoke_test,
        fp16=not smoke_test,
        save_strategy="no",
    )


def tokenizer(tokenizer_json_path) -> Tokenizer:
    """Loads a pre-trained tokenizer instance from a json file and loads all tokens."""
    my_tokenizer = PreTrainedTokenizerFast(
        tokenizer_object=Tokenizer.from_file(str(tokenizer_json_path))
    )
    my_tokenizer.mask_token = "<mask>"
    my_tokenizer.pad_token = "<pad>"
    my_tokenizer.cls_token = "<s>"
    my_tokenizer.sep_token = "</s>"
    my_tokenizer.model_max_length = 512
    my_tokenizer.is_pretokenized = False

    return my_tokenizer


def model_init(
    label_encoding: dict, model_name: str
) -> RobertaForSequenceClassification:
    """Initializes a model (pre-trained by model_name) with a given label encoding."""
    return RobertaForSequenceClassification.from_pretrained(
        model_name,
        num_labels=len(label_encoding.keys()),
        id2label={v: k for k, v in label_encoding.items()},
        label2id=dict(label_encoding),
        ignore_mismatched_sizes=True,
    )


def data_collator(tokenizer) -> DataCollatorWithPadding:
    """Initializes a data collator for training the transformer."""
    return DataCollatorWithPadding(tokenizer, padding="max_length", max_length=512)


def compute_metrics(metric, eval_pred):
    """Converts the prediction to the correct index format for metric computation."""
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return metric.compute(predictions=predictions, references=labels)


def build_huggingface_dataset(examples: list[dict], label_encoding: dict):
    """Converts the ROMEO dictionary examples into a HuggingFace dataset."""
    candidate_tokenized_examples = [example["TokenizedExample"] for example in examples]
    candidate_labels = [label_encoding[example["Label"]] for example in examples]

    huggingface_dataset = [
        {
            key: torch.tensor(val)
            for key, val in candidate_tokenized_examples[idx].items()
        }
        for idx, _ in enumerate(candidate_tokenized_examples)
    ]

    for idx, example in enumerate(huggingface_dataset):
        example["labels"] = torch.tensor(candidate_labels[idx])

    return huggingface_dataset


def obtain_predictions(
    examples: list[dict], tokenizer_json_path: str, model_path: str, smoke_test: bool
):
    """Obtains predictions for a list of examples using the given HuggingFace model and tokenzier."""
    device = "cuda:0" if (torch.cuda.is_available() and not smoke_test) else "cpu"
    my_tokenizer = tokenizer(tokenizer_json_path)
    my_data_collator = data_collator(my_tokenizer)

    model = RobertaForSequenceClassification.from_pretrained(model_path)
    model.to(device)

    dataset = build_huggingface_dataset(examples, model.config.label2id)
    my_data_loader = DataLoader(
        dataset, batch_size=16 if not smoke_test else 1, collate_fn=my_data_collator
    )

    idx = 0
    for b in my_data_loader:
        b.to(device)
        result = model(**b)
        lbl = b.labels.cpu().numpy()
        cls = result.logits.argmax(dim=1).cpu().numpy()
        for i in range(len(cls)):
            examples[idx + i]["Prediction"] = model.config.id2label[cls[i]]

            # Make sure nothing got mixed up when loading the data
            assert examples[idx + i]["Label"] == model.config.id2label[lbl[i]]
        idx += len(cls)

    return examples


__all__ = [
    "training_arguments",
    "tokenizer",
    "model_init",
    "data_collator",
    "compute_metrics",
    "build_huggingface_dataset",
    "obtain_predictions",
]
